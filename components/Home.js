import React, { useState, useEffect } from 'react';
import { View, Text, Button, Image, FlatList, StyleSheet,SafeAreaView, ScrollView } from 'react-native';

const styles = StyleSheet.create({
    contenedor: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        borderBottomWidth: 2,
        paddingVertical: 10,
        width: '100%'
    },
    imagen: {
        padding: 5
    },
    boton: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 5
    }
})

function Item(props){
    return(
        <View style={styles.contenedor}>
            <View style={styles.imagen}>
                <Image source={{uri: props.image}} style={{width: 80, height: 80, borderRadius: 80/2}}/>
            </View>
            <View style={{width: 340, height: 80, marginRight: 10, justifyContent: 'center'}}>
                <Text numberOfLines={1} style={{fontWeight: 'bold',fontSize: 20}}>{props.titulo}</Text>
                <Text numberOfLines={2} style={{fontSize: 15}}>{props.resumen}</Text>
            </View>
            <View style={styles.boton}>
                <Button
                    title=">"
                    color="#f4511e"
                    onPress={() => props.navigation.navigate('Details',{
                        imagen: props.image,
                        titulo: props.titulo,
                        resumen: props.resumen
                    })}/>
            </View>
        </View>
    )
}

function Home({ navigation }) {
    const [lista, setLista] = useState([])

    useEffect(() => {
        fetch(
          "https://yts.mx/api/v2/list_movies.json"
        )
        .then(res => res.json())
        .then(
            result => {
                setLista(result.data.movies)
            },
        )}
    )
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <FlatList
            data={lista.length > 0 ? lista : []} renderItem={({item})=>{
                return(
                <Item image={item.medium_cover_image} titulo={item.title} resumen={item.summary} navigation={navigation} />)
            }}
            keyExtractor = {item => item.id}
        />
        </View>
    );
}

export default Home