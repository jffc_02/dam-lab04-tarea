import * as React from 'react';
import { View, Text, Image, SafeAreaView, ScrollView } from 'react-native';

function Detail({ route }) {
    const { imagen,titulo,resumen } = route.params;
    return (
        <SafeAreaView>
            <ScrollView>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 30, marginBottom: 10}}>{titulo}</Text>
                <Image source={{uri: imagen}} style={{width: 380, height: 480, borderRadius: 80/2, marginBottom: 10}}/>
                <Text style={{textAlign: 'justify', width: '80%', fontSize: 20}}>{resumen}</Text>
            </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export default Detail